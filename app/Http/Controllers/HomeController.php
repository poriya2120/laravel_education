<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
class HomeController extends Controller
{
    public function getid(Request $req)
    {
        $id=$req['id'];
        return View('showid')->with('id',$id);
    }
    public function index(Request $req)
    {
        $id=$req['id'];
        // dd($req->all());
        $data='<h1>this is data<h1>';
    // //     if (view()->exists('poriya')) 
    //     {
      
       return View('poriya')->with('id',$id)->with('data',$data);
    //         # code...
    //     }
    //     else {
    //         return 'there is no views';
    //     }
       
        // return View()->first(['mohamd','poriya'])->with('data',$data);
        // return view()->first(['custom.admin', 'poriya'], $data);
        // return View::first(['custom.admin', 'poriya'], $data);
        // return View ::first(['mohamad','ali'],$data);
        // return View::first(['ali','poriya'])->with('data',$data);
        // return View ::first(['pori','poriya'],['data',$data])
    }
     public function showblock()
    {
        $data= array(
         array(
             "name" => "poriya",
             "id"=>"1",
            "contant"=>"student"
         ),
          array(
              "name" => "taha",
              "id"=>"2",
              "contant"=>"doctor" 
            );

         );
         return View('blog')->with('data',$data);
    }
    public function showpost(Request $Request)
    {   $data= array(
        array(
            "name" => "poriya",
            "id"=>"1",
           "contant"=>"student"
        ),
         array(
             "name" => "taha",
             "id"=>"2",
             "contant"=>"doctor" 
           );

        );
        $id=array_search($Request['id'],array_column($data,'id'));
        $returndata=$data[$id];
        return View('post')->with('data',$returndata);
    }
}
