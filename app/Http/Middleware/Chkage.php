<?php

namespace App\Http\Middleware;

use Closure;

class chkage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->age < 100){
            return $next($request);
        }else {
            return redirect('/');
        }
      
    }
}
