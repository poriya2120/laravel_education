<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/poriya',function(){
// return 'poriya';
// });
Route::get('/poriya','HomeController@index')->middleware('Chkage');
Route::get('/poriya/{id}','HomeController@getid');
Route::get('/blog','HomeController@showblock');
Route::get('/porst/{id}','HomeController@showpost')